﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace PlcMsgTransfer
{
    class DictionaryMessage
    {
        private Dictionary<string, SMessage> Messages;
        
        public DictionaryMessage()
        {
            Messages = new Dictionary<string, SMessage>();
            ReadMessageFromDB();
        }

        public SMessage getMessage(string MessageKey)
        {
            if (Messages.ContainsKey(MessageKey))
            {
                return Messages[MessageKey];
            }
            else
            {
                return new SMessage(-1,1,MessageKey,1,1,1,1,1,1,1);
            }
        }

        public void ReadMessageFromDB()
        {
            Messages.Clear();
            Sqldbconnection SqlDb;
            //string connectionString = "Provider=sqloledb;Data Source=localhost\\sqlexpress;Password=zaqew;User ID=orpo;Initial Catalog=SEM_MPSA;Connect Timeout=2";
            string connectionString = "Provider=" + ConfigurationManager.AppSettings.Get("Provider") +
                             ";Data Source=" + ConfigurationManager.AppSettings.Get("Data Source") +
                             ";Initial Catalog=" + ConfigurationManager.AppSettings.Get("Initial Catalog") +
                             ";Connect Timeout=" + ConfigurationManager.AppSettings.Get("Connect Timeout") +
                             ";Integrated Security=" + ConfigurationManager.AppSettings.Get("Integrated Security");
            SqlDb = new Sqldbconnection(connectionString);
            try
            {
                SqlDb.open();
                SqlDb.CreateDbCommand("Select * from Message");
                SqlDb.ExecuteReader();
                while (SqlDb.reader.Read())
                {
                    if (Convert.ToInt32(SqlDb.reader["SysID"]) < 10)
                    {
                        Messages["00" + Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["Mess"])] = new SMessage(Convert.ToInt32(SqlDb.reader["SysID"]),
                                                                Convert.ToInt32(SqlDb.reader["Mess"]), Convert.ToString(SqlDb.reader["Message"]), Convert.ToInt32(SqlDb.reader["Kind"]),
                                                                Convert.ToInt32(SqlDb.reader["Priority"]), Convert.ToInt32(SqlDb.reader["Sound"]), Convert.ToInt32(SqlDb.reader["IDSound"]),
                                                                Convert.ToInt32(SqlDb.reader["Type"]), Convert.ToInt32(SqlDb.reader["isAck"]), Convert.ToInt32(SqlDb.reader["IDColor"]));
                    }
                    else if ((Convert.ToInt32(SqlDb.reader["SysID"]) >= 10) && (Convert.ToInt32(SqlDb.reader["SysID"]) < 100))
                    {
                        Messages["0" + Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["Mess"])] = new SMessage(Convert.ToInt32(SqlDb.reader["SysID"]),
                                                                Convert.ToInt32(SqlDb.reader["Mess"]), Convert.ToString(SqlDb.reader["Message"]), Convert.ToInt32(SqlDb.reader["Kind"]),
                                                                Convert.ToInt32(SqlDb.reader["Priority"]), Convert.ToInt32(SqlDb.reader["Sound"]), Convert.ToInt32(SqlDb.reader["IDSound"]),
                                                                Convert.ToInt32(SqlDb.reader["Type"]), Convert.ToInt32(SqlDb.reader["isAck"]), Convert.ToInt32(SqlDb.reader["IDColor"]));
                    }
                    else
                    {
                        Messages[Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["Mess"])] = new SMessage(Convert.ToInt32(SqlDb.reader["SysID"]),
                                                                Convert.ToInt32(SqlDb.reader["Mess"]), Convert.ToString(SqlDb.reader["Message"]), Convert.ToInt32(SqlDb.reader["Kind"]),
                                                                Convert.ToInt32(SqlDb.reader["Priority"]), Convert.ToInt32(SqlDb.reader["Sound"]), Convert.ToInt32(SqlDb.reader["IDSound"]),
                                                                Convert.ToInt32(SqlDb.reader["Type"]), Convert.ToInt32(SqlDb.reader["isAck"]), Convert.ToInt32(SqlDb.reader["IDColor"]));
                    }
                }
                SqlDb.reader.Close();
                SqlDb.close();
            }
            catch (Exception)
            { 
            }
        }
    }
}
