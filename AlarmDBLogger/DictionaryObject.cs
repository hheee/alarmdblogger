﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;


namespace PlcMsgTransfer
{
    class DictionaryObject
    {
        private Dictionary<string, SObject> Objects;

        public DictionaryObject()
        {
            Objects = new Dictionary<string, SObject>();
            ReadObjectsFromDB();
        }

        public SObject getObject(string Objectkey)
        {
            if (Objects.ContainsKey(Objectkey))
            {
                return Objects[Objectkey];
            }
            else
            {
                return new SObject(-1,1,Objectkey,-1,-1,-1);
            }

        }

        public void ReadObjectsFromDB()
        {
            Objects.Clear();
            Sqldbconnection SqlDb;
            string connectionString = "Provider=" + ConfigurationManager.AppSettings.Get("Provider") +
                             ";Data Source=" + ConfigurationManager.AppSettings.Get("Data Source") +
                             ";Initial Catalog=" + ConfigurationManager.AppSettings.Get("Initial Catalog") +
                             ";Connect Timeout=" + ConfigurationManager.AppSettings.Get("Connect Timeout") +
                             ";Integrated Security=" + ConfigurationManager.AppSettings.Get("Integrated Security");
            SqlDb = new Sqldbconnection(connectionString);
            try
            {
                SqlDb.open();
                SqlDb.CreateDbCommand("Select * from Object");
                SqlDb.ExecuteReader();
                while (SqlDb.reader.Read())
                {
                    if (Convert.ToInt32(SqlDb.reader["SysID"]) < 10)
                    {
                        Objects["00" + Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["SysNum"])] = new SObject(Convert.ToInt32(SqlDb.reader["SysID"]),
                                                                Convert.ToInt32(SqlDb.reader["SysNum"]), Convert.ToString(SqlDb.reader["Name"]), Convert.ToInt32(SqlDb.reader["Sound1"]),
                                                                Convert.ToInt32(SqlDb.reader["Sound2"]), Convert.ToInt32(SqlDb.reader["Sound3"]));
                    }
                    else if ((Convert.ToInt32(SqlDb.reader["SysID"]) >= 10) && (Convert.ToInt32(SqlDb.reader["SysID"]) < 100))
                    {
                        Objects["0" + Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["SysNum"])] = new SObject(Convert.ToInt32(SqlDb.reader["SysID"]),
                                                                Convert.ToInt32(SqlDb.reader["SysNum"]), Convert.ToString(SqlDb.reader["Name"]), Convert.ToInt32(SqlDb.reader["Sound1"]),
                                                                Convert.ToInt32(SqlDb.reader["Sound2"]), Convert.ToInt32(SqlDb.reader["Sound3"]));
                    }
                    else
                    {
                        Objects[Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["SysNum"])] = new SObject(Convert.ToInt32(SqlDb.reader["SysID"]),
                                                                Convert.ToInt32(SqlDb.reader["SysNum"]), Convert.ToString(SqlDb.reader["Name"]), Convert.ToInt32(SqlDb.reader["Sound1"]),
                                                                Convert.ToInt32(SqlDb.reader["Sound2"]), Convert.ToInt32(SqlDb.reader["Sound3"]));
                    }
                }
                SqlDb.reader.Close();
                SqlDb.close();
            }
            catch (Exception)
            { 
            }

        }
    }
}
