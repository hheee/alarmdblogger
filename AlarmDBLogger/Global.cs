﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlcMsgTransfer
{
    static class Global
    {
        public static string TYPE = "type";
        public static string HOST = "host";
        public static string PORT = "port";
        public static string HOTSTANDBY = "hotstandby";
        public static string NAMEDB = "namedb";
        public static string PLC = "plc";
        public static string YES = "yes";
        public static string NO = "no";
        public static string ID = "id";
        public static byte a = 1;
        public static byte b = 1;
        public static string DB = "db";
        public static string ACKONCE = "1";
        public static string ACKALL = "2";
        public static string ARM = "250";
        public static string ISACK = "1";
        public static string PROVIDERL = "providerl";
        public static string DATASOURCEL = "datasourcel";
        public static string USERIDL = "useridl";
        public static string PASSWORDL = "passwordl";
        public static string INITCATALOGL = "initcatalogl";
        public static string CONNECTTIMEOUTL = "connecttimeoutl";
        public static string PROVIDERR = "providerr";
        public static string DATASOURCER = "datasourcer";
        public static string USERIDR = "useridr";
        public static string PASSWORDR = "passwordr";
        public static string INITCATALOGR = "initcatalogr";
        public static string CONNECTTIMEOUTR = "connecttimeoutr";
        public static string STRNOTACK = "Не квитировано";
    }
}
