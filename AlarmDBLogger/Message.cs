﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace modbusrep
{
    class Message
    {
        private int _SysID;
        private int _KodMess;
        private string _Mess;
        private int _Kind;
        private int _Priority;
        private int _Sound;
        private int _IDSound;
        private int _Type;
        private int _isAck;
        private int _IDColor;

        public Message()
        { 
        }

        public Message(int setSysID, int setKodMess, string setMess, int setKind, int setPriority, int setSound, int setIDSound, int setType, int setisAck, int setIDColor)
        {
            _SysID = setSysID;
            _KodMess = setKodMess;
            _Mess = setMess;
            _Kind = setKind;
            _Priority = setPriority;
            _Sound = setSound;
            _IDSound = setIDSound;
            _Type = setType;
            _isAck = setisAck;
            _IDColor = setIDColor;
        }

        public int SysID
        {
            set { _SysID = value; }
            get { return _SysID; }
        }

        public int KodMess
        {
            set { _KodMess = value; }
            get { return _KodMess; }
        }

        public string Mess
        {
            set { _Mess = value; }
            get { return _Mess; }
        }

        public int Kind
        {
            set { _Kind = value; }
            get { return _Kind; }
        }

        public int Priority
        {
            set { _Priority = value; }
            get { return _Priority; }
        }

        public int Sound
        {
            set { _Sound = value; }
            get { return _Sound; }
        }

        public int IDSound
        {
            set { _IDSound = value; }
            get { return _IDSound; }
        }

        public int Type
        {
            set { _Type = value; }
            get { return _Type; }
        }

        public int isAck
        {
            set { _isAck = value; }
            get { return _isAck; }
        }

        public int IDColor
        {
            set { _IDColor = value; }
            get { return _IDColor; }
        }
    }
}
