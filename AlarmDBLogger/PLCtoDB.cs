﻿using System;
using System.Net;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
//using ModbusTCP;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Data.OleDb;

namespace modbusrep
{
    public class Threads
    {
        private Dictionary<string, ChangeUI> _oper;
        private List<Thread> _threads; // создаем динамический список
        private string name;
        public Threads(Dictionary<string, ChangeUI> oper) //конструктор будет принимать в себя ссылку на метод
        {
            _oper = oper;
        }

        private void MBmaster_OnException(ushort id, byte unit, byte function, byte exception)
        {
            string exc = "Modbus says error: ";
            switch (exception)
            {
                case modbus.excIllegalFunction: exc += "Illegal function!"; break;
                case modbus.excIllegalDataAdr: exc += "Illegal data adress!"; break;
                case modbus.excIllegalDataVal: exc += "Illegal data value!"; break;
                case modbus.excSlaveDeviceFailure: exc += "Slave device failure!"; break;
                case modbus.excAck: exc += "Acknoledge!"; break;
                case modbus.excGatePathUnavailable: exc += "Gateway path unavailbale!"; break;
                case modbus.excExceptionTimeout: exc += "Slave timed out!"; break;
                case modbus.excExceptionConnectionLost: exc += "Connection is lost!"; break;
                case modbus.excExceptionNotConnected: exc += "Not connected!"; break;
            }

            _oper["log"](Thread.CurrentThread.Name, exc);
            //MessageBox.Show(exc, "Modbus slave exception");
        }

        public void Run(List<string> names, List<Device> devices)
        {
            _threads = new List<Thread>();
            for (int i = 0; i < devices.Count; i++)
            {
                if (devices[i].getProperty(Global.TYPE) == Global.PLC)
                {
                    name = names[i];

                    var thread = new Thread(multiThread);
                    thread.Name = name;
                    thread.IsBackground = true; // закрываем форму и все потоки уничтожаются
                    _threads.Add(thread); // добавляем наш поток в динамический список
                    _threads[i].Start(devices[i]); // запускаем наш поток
                }

                if (devices[i].getProperty(Global.TYPE) == "db")
                {
                    name = names[i];
                    var thread = new Thread(multiThread);
                    thread.Name = name;
                    thread.IsBackground = true; // закрываем форму и все потоки уничтожаются
                    _threads.Add(thread); // добавляем наш поток в динамический список
                    _threads[i].Start(devices[i]); // запускаем наш поток
                }
                Thread.Sleep(100);
            }

        }

        public void Abort()
        {
            foreach (Thread thread in _threads)
            {
                thread.Abort(); // уничтожаем поток
            }
            _threads.Clear(); // очищаем список
        }


        private string WriteMessToDB(Sqldbconnection SqlDb, int iStart, int iStop, byte[] Butt, string DateTime, Int32 IDmess, Dictionary<string,string> objects)
        {
            
            byte[] _Butt = Butt;
            byte SysID;                                                                             //Идентификатор объекта
            byte Mess;                                                                              //Код сообщения    
            Int16 SysNum;                                                                           //Номер объекта
            float Val;                                                                              //Значение

            for (int jj = iStart; jj <= iStop; jj++)
            {

                SysID = _Butt[10 + (jj - 1) * 8];
                if (SysID == 255)
                {
                    DateTime = "";
                    if (_Butt[14 + (jj - 1) * 8] < 10)
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[14 + (jj - 1) * 8]) + ".";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[14 + (jj - 1) * 8]) + ".";
                    }

                    if (_Butt[13 + (jj - 1) * 8] < 10)
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[13 + (jj - 1) * 8]) + ".";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[13 + (jj - 1) * 8]) + ".";
                    }

                    DateTime = DateTime + "20" + Convert.ToString(_Butt[12 + (jj - 1) * 8]) + " ";

                    if (_Butt[15 + (jj - 1) * 8] < 10)
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[15 + (jj - 1) * 8]) + ":";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[15 + (jj - 1) * 8]) + ":";
                    }

                    if (_Butt[16 + (jj - 1) * 8] < 10)
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[16 + (jj - 1) * 8]) + ":";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[16 + (jj - 1) * 8]) + ":";
                    }

                    if (_Butt[17 + (jj - 1) * 8] < 10)
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[17 + (jj - 1) * 8]);
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[17 + (jj - 1) * 8]);
                    }

                }
                else
                {
                    Mess = _Butt[11 + (jj - 1) * 8];
                    //Array.Reverse(_Butt, 12 + (jj - 1) * 8, sizeof(Int16));
                    SysNum = BitConverter.ToInt16(_Butt, 12 + (jj - 1) * 8);
                    //Array.Reverse(_Butt, 14 + (jj - 1) * 8, sizeof(Single));
                    Val = BitConverter.ToSingle(_Butt, 14 + (jj - 1) * 8);
                    SqlDb.CreateDbCommand("INSERT INTO PLCMessage (IDmess, Place, DTime, DTimeAck, SysID, SysNum, Mess, Value, ObjectName) VALUES (?,?,?,?,?,?,?,?,?)");
                    SqlDb.command.Parameters.Add("IDmess", OleDbType.Decimal).Value = IDmess;
                    SqlDb.command.Parameters.Add("Place", OleDbType.VarChar).Value = "PLC";
                    SqlDb.command.Parameters.Add("DTime", OleDbType.VarChar).Value = DateTime;
                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                    SqlDb.command.Parameters.Add("SysID", OleDbType.Integer).Value = SysID;
                    SqlDb.command.Parameters.Add("SysNum", OleDbType.Integer).Value = SysNum;
                    SqlDb.command.Parameters.Add("Mess", OleDbType.Integer).Value = Mess;
                    SqlDb.command.Parameters.Add("Value", OleDbType.VarChar).Value = Val;
                    if (objects.ContainsKey(Convert.ToString(SysID) + Convert.ToString(SysNum)))
                    {
                        SqlDb.command.Parameters.Add("ObjectName", OleDbType.VarChar).Value = objects[Convert.ToString(SysID) + Convert.ToString(SysNum)];
                    }
                    else
                    {
                        SqlDb.command.Parameters.Add("ObjectName", OleDbType.VarChar).Value = "";
                    }
                    SqlDb.command.ExecuteNonQuery();
                }
            }
            return DateTime;


        }
        // ------------------------------------------------------------------------
        // Modbus TCP slave exception
        // ------------------------------------------------------------------------


        private void multiThread(object dev)
        {
            Device d = (Device)dev;                                                                 //Объект тип устройства
            Sqldbconnection SqlDb;
            int i = 0;
            modbus modbusTCP = null;                                                                //Объект типа Модбас
            modbusTCP = new modbus();
            string ip = d.getProperty(Global.HOST);
            ushort port = (ushort)Convert.ToInt16(d.getProperty(Global.PORT));
            ushort ID = 3;                                                                          //3 функция модбас
            byte unit = 1;                                                                          //ID устройства
            //byte[] _Point = new byte[250];
            //byte[] value = new byte[250];
            string connectionString = "Provider=SQLOLEDB;Data Source=PROG\\SQLEXPRESS;Password=sa;User ID=sa;Initial Catalog=OperMess; Connect Timeout = 1";
            SqlDb = new Sqldbconnection(connectionString);
            Dictionary<string, string> Objects = new Dictionary<string, string>();
            Dictionary<int, ushort> Butt = new Dictionary<int, ushort>();                           //Словарь для хранения бочек и их адресов
            byte NOldButt = 1;                                                                      //Номер последней считанной бочки
            byte NNewButt = 1;                                                                      //Номер новой бочки
            Int32 NOldMess = 0;                                                                     //Номер последнего прочитанного сообщения глобальный
            Int32 NNewMess = 0;                                                                     //Номер нового сообщения глобальный
            byte NOldMess_loc = 0;                                                                  //Номер последнего прочитанного сообщения локальный (макс = Нсооб*Нбочки)
            byte NNewMess_loc = 0;                                                                  //Номер нового сообщения лок4альный
            byte NOldMess_butt = 0;                                                                 //Номер последнего прочитанного сообщения в бочке
            byte NNewMess_butt = 0;                                                                 //Номер нового сообщения в бочке
            string DateTime = "Null";                                                               //Метка времени
            byte[] _Butt = new byte[250];                                                           //Массив данных получаемых по Модбас
            Int32 i_Start;                                                                          //Старт для цикла
            Int32 i_Stop;                                                                           //Стоп для цикла
            byte[] Nmessout = new byte[18];                                                         //Массив для выдачи сообщений о пропусках
            modbusTCP.OnException += new modbus.ExceptionData(MBmaster_OnException);
            
            try                        
            {                            
                if (!SqlDb.Sqlconnected())                            
                {                                
                    SqlDb.open();                            
                }                        
            }                        
            catch (Exception ex)                        
            {                            
                _oper["log"](Thread.CurrentThread.Name, ex.Message); // по ссылке _ui передаем значение в нашу форму                        
            }
            Thread.Sleep(10);           
            SqlDb.CreateDbCommand("SELECT NButt, AdrButt FROM ButtSetup");                        
            SqlDb.ExecuteReader();                        
            while (SqlDb.reader.Read())                        
            {                            
                Butt[Convert.ToByte(SqlDb.reader["NButt"])] = (ushort)Convert.ToUInt16(SqlDb.reader["AdrButt"]);                        
            }                        
            SqlDb.reader.Close();

            SqlDb.CreateDbCommand("SELECT SysID, SysNum, Name FROM Object");
            SqlDb.ExecuteReader();
            while (SqlDb.reader.Read())
            {
                Objects[Convert.ToString(SqlDb.reader["SysID"])+Convert.ToString(SqlDb.reader["SysNum"])] = Convert.ToString(SqlDb.reader["Name"]);
            }
            SqlDb.reader.Close();    

            SqlDb.CreateDbCommand("SELECT TOP (1) IDmess FROM PLCMessage WHERE (Place = 'PLC') ORDER BY ID DESC");                        
            SqlDb.ExecuteReader();                        
            while (SqlDb.reader.Read())                        
            {                            
                if (SqlDb.reader["IDmess"] != null)                            
                {                                
                    NOldMess = Convert.ToInt32(SqlDb.reader["IDmess"]);                                
                    NOldMess_loc = (byte)((NOldMess) - ((NOldMess) / 240) * 240);                                
                    if ((NOldMess_loc - (NOldMess_loc / 30) * 30) == 0)                                
                    {                                    
                        NOldButt = (byte)(NOldMess_loc / 30);                                
                    }                                
                    else                                
                    {                
                        NOldButt = (byte)((NOldMess_loc / 30) + 1);                                
                    }                                
                    NOldMess_butt = (byte)(NOldMess_loc - (NOldMess_loc / 30) * 30);                           
                }                            
                else                            
                {                                
                    NOldMess = 0;                               
                    NOldMess_loc = 0;                                
                    NOldMess_butt = 0;                                
                    NOldButt = 0;                            
                }                        
            }         
            SqlDb.reader.Close();



            while (true)
            {
                //MessageBox.Show(Convert.ToString(SqlDb.connection.State));
                _oper["status"](Thread.CurrentThread.Name, Convert.ToString(modbusTCP.Socketconnected()) + " " + Convert.ToString(i) + " count= " + Convert.ToString(modbusTCP.count)); // по ссылке _ui передаем значение в нашу форму

                try
                {
                    if (!SqlDb.Sqlconnected())
                    {
                        SqlDb.open();
                    }
                }
                catch
                {
                    continue;
                }

                try
                {
                    if (d.getProperty(Global.HOTSTANDBY) == Global.YES)
                    {
                        if (!modbusTCP.Socketconnected())
                        {
                            if (d.getProperty(Global.ID) == "1")
                            {
                                Global.a = 2;
                            }
                            else
                            {
                                Global.a = 1;
                            }
                            modbusTCP.connect(ip, port);
                        }
                        else
                        {
                            if (d.getProperty(Global.ID) == "1")
                            {
                                Global.a = 1;
                            }
                            else
                            {
                                if (Global.a == 1)
                                {
                                    Global.b = 1;
                                }
                                if (Global.a == 2)
                                {
                                    Global.b = 2;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    //_oper["log"](Thread.CurrentThread.Name, error.Message); // по ссылке _ui передаем значение в нашу форму
                    //MessageBox.Show(error.Message);
                    continue;
                }
                if (((d.getProperty(Global.HOTSTANDBY) == Global.YES) && (((d.getProperty(Global.ID) == "1") && (Global.a == 1) && (Global.b == 1)) || ((d.getProperty(Global.ID) == "2") && (Global.a == 2) && (Global.b == 2)))) || (d.getProperty(Global.HOTSTANDBY) == Global.NO))
                {
                    if (d.getProperty(Global.TYPE) == Global.PLC) i++;
                    modbusTCP.frame = 0;
                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[NOldButt], 125, ref _Butt);
                    if (_Butt == null) continue;
                    //Array.Reverse(_Butt, 0, sizeof(Int32));
                    //Array.Reverse(_Butt, 0, sizeof(Int16));
                    //Array.Reverse(_Butt, 2, sizeof(Int16));
                    NNewMess = BitConverter.ToInt32(_Butt, 0);
                    //MessageBox.Show(Convert.ToString(NNewMess));
                    NNewMess_loc = (byte)((NNewMess) - ((NNewMess) / 240) * 240);
                    if ((NNewMess_loc - (NNewMess_loc / 30) * 30) == 0)
                    {
                        NNewButt = (byte)(NNewMess_loc / 30);
                    }
                    else
                    {
                        NNewButt = (byte)((NNewMess_loc / 30) + 1);
                    }
                    NNewMess_butt = (byte)(NNewMess_loc - (NNewMess_loc / 30) * 30);

                    if ((NNewMess - NOldMess) >= 240)
                    {
                        Nmessout[10] = 254; // SysId
                        Nmessout[11] = 1; // Mess
                        Array.Copy(BitConverter.GetBytes((Int16)1), 0, Nmessout, 12, 2);
                        Array.Copy(BitConverter.GetBytes((Single)((NNewMess - NOldMess) - 240)), 0, Nmessout, 14, 4);
                        DateTime = WriteMessToDB(SqlDb, 1, 1, Nmessout, DateTime, NNewMess, Objects);
                        if ((NNewButt == (byte)Butt.Count) && (NNewMess_butt == 30))
                        {
                            for (int j = 1; j <= Butt.Count; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                if (_Butt == null) continue;
                                DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                            }
                        }
                        else
                        {
                            if (NNewMess_butt == 30)
                            {
                                i_Start = NNewButt + 1;
                                i_Stop = Butt.Count;
                            }
                            else
                            {
                                i_Start = NNewButt;
                                i_Stop = Butt.Count;
                            }
                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                if (j > NNewButt)
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, NNewMess_butt + 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                            i_Start = 1;
                            i_Stop = NNewButt;

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                if (_Butt == null) continue;
                                if (j < NNewButt)
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                    }
                    else if ((NNewMess - NOldMess) != 0)
                    {
                        if ((NOldButt == Butt.Count) && (NOldMess_butt == 30))
                        {
                            i_Start = 1;
                            i_Stop = NNewButt;

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                if (_Butt == null) continue;
                                if (j < NNewButt)
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                        else if (NNewMess_loc > NOldMess_loc)
                        {
                            if (NOldMess_butt == 30)
                            {
                                i_Start = NOldButt + 1;
                                i_Stop = NNewButt;
                            }
                            else
                            {
                                i_Start = NOldButt;
                                i_Stop = NNewButt;
                            }
                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                if ((j > NOldButt) && (j < NNewButt))
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else if ((j == NOldButt) && (j == NNewButt))
                                {
                                    DateTime = WriteMessToDB(SqlDb, NOldMess_butt + 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                                else if ((j == NOldButt))
                                {
                                    DateTime = WriteMessToDB(SqlDb, NOldMess_butt + 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else if ((j == NNewButt))
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                        else if (NNewMess_loc < NOldMess_loc)
                        {
                            if (NOldMess_butt == 30)
                            {
                                i_Start = NOldButt + 1;
                                i_Stop = Butt.Count;
                            }
                            else
                            {
                                i_Start = NOldButt;
                                i_Stop = Butt.Count;
                            }

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                if (j > NOldButt)
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, NOldMess_butt + 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                            }

                            i_Start = 1;
                            i_Stop = NNewButt;

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt);
                                if (_Butt == null) continue;
                                if (j < NNewButt)
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                    }
                    NOldButt = NNewButt;
                    NOldMess = NNewMess;
                    NOldMess_butt = NNewMess_butt;
                    NOldMess_loc = NNewMess_loc;

                    //for (int j = 0; j < value.Length; j++)
                    //{
                    //    MessageBox.Show(Convert.ToString(value[j]));
                    //}
                    //MessageBox.Show(Convert.ToString(value.Length));

                    //_oper["log"](Thread.CurrentThread.Name, Convert.ToString(modbusTCP.Socketconnected()) + " " + Convert.ToString(i) + " count= " + Convert.ToString(modbusTCP.count)); // по ссылке _ui передаем значение в нашу форму
                    Thread.Sleep(100);
                }
            }
        }
    }
}
