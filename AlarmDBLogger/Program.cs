﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

namespace PlcMsgTransfer
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Process[] AllProcessesAlarms = Process.GetProcessesByName("PlcLogTransport");
            Process CurrentProces = Process.GetCurrentProcess();
            foreach (Process Index in AllProcessesAlarms)
                if (Index.Id != CurrentProces.Id)
                {
                    WINAPI.SendMessage(Index.MainWindowHandle, WINAPI.WM_OPEN, 0, 0);
                    WINAPI.SetForegroundWindow(Index.MainWindowHandle);
                    return;
                }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new main());
        }
    }
}
