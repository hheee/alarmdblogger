﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using Microsoft.Win32;

namespace PlcMsgTransfer
{
    public class Sqldbconnection
    {
        private string _connectionstr = "";
        private string _Provider;
        private string _DataSource;
        private string _IntegratedSecurity;
        private static string _InitialCatalog;
        private static string _TimeOut;
        private static string _TableName;
        private static string _MessErr = "No Error";
        private OleDbConnection _connection;
        private OleDbCommand _command;
        private OleDbDataReader _reader;

        //RegistryKey regKey32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Active\\NPS\\v1.0\\SettingDB");
        //RegistryKey regKey64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64).OpenSubKey("SOFTWARE\\Active\\NPS\\v1.0\\SettingDB");
        public Sqldbconnection()
        {
            /*if (regKey64 != null)
            {
                _Provider = (string)regKey64.GetValue("Provider");
                _DataSource = (string)regKey64.GetValue("DataSource");
                _UserId = (string)regKey64.GetValue("UserId");
                _Password = (string)regKey64.GetValue("Password");
                _InitialCatalog = (string)regKey64.GetValue("InitialCatalog");
                _TimeOut = (string)regKey64.GetValue("TimeOut");
            }
            else if (regKey32 != null)
            {
                _Provider = (string)regKey32.GetValue("Provider");
                _DataSource = (string)regKey32.GetValue("DataSource");
                _UserId = (string)regKey32.GetValue("UserId");
                _Password = (string)regKey32.GetValue("Password");
                _InitialCatalog = (string)regKey32.GetValue("InitialCatalog");
                _TimeOut = (string)regKey32.GetValue("TimeOut");
            }
            else
            {*/
                _Provider = "SQLOLEDB";
                _DataSource = "FILATOV\\SQLSERVER";
                _IntegratedSecurity = "SSPI";
                _InitialCatalog = "SEM_MPSA";
                _TimeOut = "2";
            //}


        }

        public Sqldbconnection(string connectionstr)
        {
            this.connectionstr = connectionstr;
        }

        public string connectionstr
        {
            get { return _connectionstr; }
            set { _connectionstr = value; }
        }

        public string Provider
        {
            get { return _Provider; }
            set { _Provider = value; }
        }

        public string DataSource
        {
            get { return _DataSource; }
            set { _DataSource = value; }
        }

        public string IntegratedSecurity
        {
            get { return _IntegratedSecurity; }
            set { _IntegratedSecurity = value; }
        }


        public string InitialCatalog
        {
            get { return _InitialCatalog; }
            set { _InitialCatalog = value; }
        }

        public string TimeOut
        {
            get { return _TimeOut; }
            set { _TimeOut = value; }
        }

        public string TableName
        {
            get { return _TableName; }
            set { _TableName = value; }
        }

        public string MessErr
        {
              get { return _MessErr; }
              set { _MessErr = value; }
        }

        public OleDbConnection connection
        {
            get { return _connection; }
        }

        public OleDbCommand command
        {
            get { return _command; }
        }

        public OleDbDataReader reader
        {
            get { return _reader; }
        }

        public bool Sqlconnected()
        {
            if (_connection == null)
            {
                return false;
            }
            else if (_connection.State.Equals(System.Data.ConnectionState.Closed))
            {
                return false;
            }
            else if (_connection.State.Equals(System.Data.ConnectionState.Open))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void open()
        {
            if (this.connectionstr == "")
            {
                this.connectionstr = "Provider=" + this.Provider + "; Data Source=" + this.DataSource + "; Integrated Security=" + this.IntegratedSecurity +
                                   "; Initial Catalog=" + this.InitialCatalog + "; TimeOut=" + this.TimeOut;
            }
            if (_connection == null)
            {
                _connection = new OleDbConnection(_connectionstr);
            }
            _connection.Open();
        }

        public void CreateDbCommand(string queryString)
        {
            _command = _connection.CreateCommand();
            _command.CommandText = queryString;
        }

        public void ExecuteReader()
        {
            _reader = _command.ExecuteReader();
        }

        public void close()
        {
            _connection.Close();
        }

    }
    
    
    /*
    class Sqldbconnection
    {
        private string _connectionstr;
        private OleDbConnection _connection;
        private OleDbCommand _command;
        private OleDbDataReader _reader;

        public Sqldbconnection(string connectionstr)
        {
            _connectionstr = connectionstr;
        }

        public string connectionstr
        {
            get { return _connectionstr; }
        }
        
        public OleDbConnection connection
        {
            get { return _connection; }
        }

        public OleDbCommand command
        {
            get { return _command; }
        }

        public OleDbDataReader reader
        {
            get { return _reader; }
        }

        public bool Sqlconnected()
        {
            if (_connection == null)
            {
                return false;
            }
            else if (_connection.State.Equals(System.Data.ConnectionState.Closed))
            {
                return false;
            }
            else if (_connection.State.Equals(System.Data.ConnectionState.Open))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void open()
        {
                if (_connection == null)
                {
                    _connection = new OleDbConnection(_connectionstr);
                }
                _connection.Open();
        }

        public void CreateDbCommand(string queryString)
        {
            _command = _connection.CreateCommand();
            _command.CommandText = queryString;
        }

        public void ExecuteReader()
        {
            _reader = _command.ExecuteReader();
        }

        public void close()
        {
            _connection.Close();
        }

    }
     */
}
