﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace PlcMsgTransfer
{
    class WINAPI
    {
        /// <summary>Служит для отправки пользовательских сообщений</summary>
        public const int WM_USER = 0x8000;

        /// <summary>пользовательских сообщений</summary>
        public const int WM_OPEN = WM_USER + 1;
        public const int WM_CLOSE = WM_USER + 2;
        public const int WM_ENABLE = WM_USER + 3;
        public const int WM_DISABLE = WM_USER + 4;

        public static readonly IntPtr HWND_BROADCAST = new IntPtr(0xffff);
        public const int SW_HIDE = 0;
        public const int SW_SHOW = 5;
        public const int WM_SETTINGCHANGE = 0x1a;
        public const int SMTO_ABORTIFHUNG = 0x0002;

        /// <summary>Найти дескриптор окна</summary>
        /// <param name="lpClassName">Имя класса</param>
        /// <param name="lpWindowName">Имя окна</param>
        /// <returns>Handle окна</returns>
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string ClassName, string WindowName);

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string className, string windowName);

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SendMessageTimeout(IntPtr hWnd, int Msg, IntPtr wParam, string lParam, uint fuFlags, uint uTimeout, IntPtr lpdwResult);

        /// <summary>Служит для выставления окошек на передний план</summary>
        /// <param name="hWnd">Указатель на окошко</param>
        /// <returns>ну, что-то вернёт. Видимо удалось/не удалось</returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>Отправить сообщение</summary>
        /// <param name="hWnd">Указатель на окно</param>
        /// <param name="Msg">Сообщение, например WM_USER</param>
        /// <param name="wParam">Первый параметр</param>
        /// <param name="lParam">Второй параметр</param>
        /// <returns>An application should return zero if it processes this message.</returns>
        [DllImport("user32.dll")]
        public static extern uint SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("kernel32.dll")]
        public static extern int GlobalAddAtom(StringBuilder Str);

        [DllImport("kernel32.dll")]
        public static extern int GlobalGetAtomName(int nAtom, StringBuilder lpBuffer, int nSize);

        [DllImport("kernel32.dll")]
        public static extern int GlobalDeleteAtom(int nAtom);
    }
}
