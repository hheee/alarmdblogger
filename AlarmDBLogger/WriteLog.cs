﻿using System;
using System.IO;
using Microsoft.Win32;

namespace PlcMsgTransfer
{
    class WriteLog
    {
        private string LogFile;
        public WriteLog()
        {
            LogFile = "LogFile_PlcMsgTransfer.log";
        }

        public void WriteLogToFile(Exception message)
        {
            StreamWriter file = new StreamWriter(LogFile, true);
            file.WriteLine("[" + DateTime.Now + "] " + message.InnerException + ":" +
                            message.Message + ":" + message.Source + ":" + message.TargetSite + "\n");
            file.Close();
        }

        public void WriteLogToFile(string message)
        {
            StreamWriter file = new StreamWriter(LogFile, true);
            file.WriteLine("[" + DateTime.Now + "] " + message + "\n");
            file.Close();
        }
    }
}
