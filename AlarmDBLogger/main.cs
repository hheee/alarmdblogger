﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration; //Для возможности использования app.config
using System.Data.OleDb;
using NLog;

namespace PlcMsgTransfer
{
    //public delegate void SendDataToHMIDelegate(int Table, int Line, string Value);
    //public delegate void LogDelegate(string Msg);

   
    public partial class main : Form
    {
        private static Logger loggerMsgManager = LogManager.GetCurrentClassLogger();
        private static string IP_PLC_Current= ""; //текущий опрашиваемый ip Плк

        public static uint Modbus_Count_All=0; private static uint Modbus_Count_Good = 0; private static uint Modbus_Count_Error = 0; //Для учета статистики


        private Thread thread; //поток чтения данных с плк
        private DictionaryObject Objects;
        private DictionaryMessage Messages;

        #region Реализация возможности логирование сообщений с потоков в файл
        //WriteLog WriteLog = new WriteLog();
        public void SendLogFunc(string Msg)
        {
            //if (this.InvokeRequired)
            //{
            //    this.Invoke(new LogDelegate(SendLogFunc));
            //    return;
            //}

            loggerMsgManager.Debug(Msg);
            //WriteLog.WriteLogToFile(Msg);
        }
        #endregion
        

        #region Реализация вывод диагностики на экран
        private void SendDataToHMI(int Table, int Line, string Value)
        {
            if (Table == 1) { dataGridView_diag.Rows[Line].Cells[1].Value = Value; }
            if (Table == 3) { dataGridView_modbustcp.Rows[Line].Cells[1].Value = Value; }
        }
        #endregion

        public main()
        {
            
            InitializeComponent();
            SendLogFunc("Инициализация приложения");
            
            dataGridViewBd.Rows.Add(6);
            dataGridViewBd.Rows[0].Cells[0].Value = "БД драйвер (Provider)";        dataGridViewBd.Rows[0].Cells[1].Value = ConfigurationManager.AppSettings.Get("Provider");
            dataGridViewBd.Rows[1].Cells[0].Value = "Источник (Data Source)";       dataGridViewBd.Rows[1].Cells[1].Value = ConfigurationManager.AppSettings.Get("Data Source");
            dataGridViewBd.Rows[2].Cells[0].Value = "Пароль (Password)";            dataGridViewBd.Rows[2].Cells[1].Value = "************";
            dataGridViewBd.Rows[3].Cells[0].Value = "Учетная запись (User ID)";     dataGridViewBd.Rows[3].Cells[1].Value = ConfigurationManager.AppSettings.Get("User ID");
            dataGridViewBd.Rows[4].Cells[0].Value = "Имя БД (Initial Catalog)";     dataGridViewBd.Rows[4].Cells[1].Value = ConfigurationManager.AppSettings.Get("Initial Catalog");
            dataGridViewBd.Rows[5].Cells[0].Value = "Таймаут (Connect Timeout)";    dataGridViewBd.Rows[5].Cells[1].Value = ConfigurationManager.AppSettings.Get("Connect Timeout");

            // Table 1
            dataGridView_diag.Rows.Add(9);
            dataGridView_diag.Rows[0].Cells[0].Value = "Состояние связи с ПЛК";
            dataGridView_diag.Rows[1].Cells[0].Value = "Текущий IP адрес ПЛК";      
            dataGridView_diag.Rows[2].Cells[0].Value = "Счетчик запросов";
            dataGridView_diag.Rows[3].Cells[0].Value = "Счетчик ответов";
            dataGridView_diag.Rows[4].Cells[0].Value = "Счетчик ошибок";
            dataGridView_diag.Rows[5].Cells[0].Value = "Состояние связи с БД сообщений";
            dataGridView_diag.Rows[6].Cells[0].Value = "Состояние связи с БД конфигурации сообщений";
            dataGridView_diag.Rows[7].Cells[0].Value = "Размер талицы PLCMessage";
            dataGridView_diag.Rows[8].Cells[0].Value = "Чтение конфигурации";

            // Table 3
            dataGridView_modbustcp.Rows.Add(11);
            dataGridView_modbustcp.Rows[0].Cells[0].Value = "IP адрес ПЛК Primary основной"; dataGridView_modbustcp.Rows[0].Cells[1].Value = ConfigurationManager.AppSettings.Get("IP_PLC_Primary_1");
            dataGridView_modbustcp.Rows[1].Cells[0].Value = "IP адрес ПЛК Primary резервного"; dataGridView_modbustcp.Rows[1].Cells[1].Value = ConfigurationManager.AppSettings.Get("IP_PLC_Primary_2");
            dataGridView_modbustcp.Rows[2].Cells[0].Value = "Адрес 1 элемента буфера"; dataGridView_modbustcp.Rows[2].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 1 buffer");
            dataGridView_modbustcp.Rows[3].Cells[0].Value = "Адрес 2 элемента буфера"; dataGridView_modbustcp.Rows[3].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 2 buffer");
            dataGridView_modbustcp.Rows[4].Cells[0].Value = "Адрес 3 элемента буфера"; dataGridView_modbustcp.Rows[4].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 3 buffer");
            dataGridView_modbustcp.Rows[5].Cells[0].Value = "Адрес 4 элемента буфера"; dataGridView_modbustcp.Rows[5].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 4 buffer");
            dataGridView_modbustcp.Rows[6].Cells[0].Value = "Адрес 5 элемента буфера"; dataGridView_modbustcp.Rows[6].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 5 buffer");
            dataGridView_modbustcp.Rows[7].Cells[0].Value = "Адрес 6 элемента буфера"; dataGridView_modbustcp.Rows[7].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 6 buffer");
            dataGridView_modbustcp.Rows[8].Cells[0].Value = "Адрес 7 элемента буфера"; dataGridView_modbustcp.Rows[8].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 7 buffer");
            dataGridView_modbustcp.Rows[9].Cells[0].Value = "Адрес 8 элемента буфера"; dataGridView_modbustcp.Rows[9].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus address of the 8 buffer");
            dataGridView_modbustcp.Rows[10].Cells[0].Value = "Частота опроса буфера ПЛК при отсутствии новых сообщений"; dataGridView_modbustcp.Rows[10].Cells[1].Value = ConfigurationManager.AppSettings.Get("Modbus period reading");

        }




        protected override void WndProc(ref Message m)
        {
            try
            {
                if (WINAPI.WM_USER < m.Msg && m.Msg < WINAPI.WM_USER + 10)
                    switch (m.Msg)
                    {
                        case WINAPI.WM_OPEN:
                            this.Location = new Point((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2,
                                                      (Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                            this.Activate();
                            break;
                        case WINAPI.WM_CLOSE:
                            this.Close();
                            break;
                    }
            }
            catch (Exception)
            {
            }
            base.WndProc(ref m);
        }
        

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void main_Shown(object sender, EventArgs e)
        {
            Run();
        }

        private void main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }   
        }

        private void close_Click(object sender, EventArgs e)
        {
            if (MsgBox.Show("Закрытие остановит формирование технологических сообщений. Продолжить закрытие?", "Подтверждение перед закрытием приложения", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                SendLogFunc("Приложение закрыто пользователем");
            Application.Exit();
        }

        private void main_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }




        private void MBmaster_OnException(ushort id, byte unit, byte function, byte exception)
        {
            string exc = "ModbusTCP ошибка: ";
            switch (exception)
            {
                case modbus.excIllegalFunction: exc += "Illegal function!"; break;
                case modbus.excIllegalDataAdr: exc += "Illegal data adress!"; break;
                case modbus.excIllegalDataVal: exc += "Illegal data value!"; break;
                case modbus.excSlaveDeviceFailure: exc += "Slave device failure!"; break;
                case modbus.excAck: exc += "Acknoledge!"; break;
                case modbus.excGatePathUnavailable: exc += "Gateway path unavailbale!"; break;
                case modbus.excExceptionTimeout: exc += "Slave timed out!"; break;
                case modbus.excExceptionConnectionLost: exc += "Нет связи с ПЛК"; break;
                case modbus.excExceptionNotConnected: exc += "Нет соединения с ПЛК"; break;
            }
            SendLogFunc(exc);
            // Смена IP адресов. 
            if (IP_PLC_Current == ConfigurationManager.AppSettings.Get("IP_PLC_Primary_1"))
            {
                IP_PLC_Current = ConfigurationManager.AppSettings.Get("IP_PLC_Primary_2");
            }
            else { IP_PLC_Current = ConfigurationManager.AppSettings.Get("IP_PLC_Primary_1"); }
        }

        public void Run()
        {
            try
            {
                Objects = new DictionaryObject();
                Messages = new DictionaryMessage();

                thread = new Thread(PLCtoDB);
                thread.Name = "PlcReader";
                thread.IsBackground = true;
                thread.Start(1);
                Thread.Sleep(100);
            }
            catch (Exception ex) { SendLogFunc(ex.Message); }
        }


        public void Abort()
        {
            thread.Abort(); // уничтожаем поток
        }


        private string WriteMessToDB(Sqldbconnection SqlDb, int iStart, int iStop, byte[] Butt, string DateTime, Int32 IDmess, Dictionary<string, string> objects)
        {
            byte[] _Butt = Butt;
            byte SysID;                                                                             //Идентификатор объекта
            byte Mess;                                                                              //Код сообщения    
            Int16 SysNum;                                                                           //Номер объекта
            float Val;                                                                              //Значение
                                                                                                    //string DT = Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond);
                                                                                                    //MessageBox.Show(Convert.ToString(SqlDb.connection.State));
                                                                                                    //MessageBox.Show(Convert.ToString(SqlDb.connection.ConnectionString));




            for (int jj = iStart; jj <= iStop; jj++)
            {

                SysID = _Butt[10 + (jj - 1) * 8];

                if (SysID == 255)
                {
                    DateTime = "";


                    #region преобразование времени новое

                    if (_Butt[12 + (jj - 1) * 8] < 10)//год
                    {
                        DateTime = DateTime + "200" + Convert.ToString(_Butt[12 + (jj - 1) * 8]) + "-";
                    }
                    else
                    {
                        DateTime = DateTime + "20" + Convert.ToString(_Butt[12 + (jj - 1) * 8]) + "-";
                    }
                    if (_Butt[13 + (jj - 1) * 8] < 10)//месяц
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[13 + (jj - 1) * 8]) + "-";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[13 + (jj - 1) * 8]) + "-";
                    }
                    if (_Butt[14 + (jj - 1) * 8] < 10)//день
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[14 + (jj - 1) * 8]) + " ";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[14 + (jj - 1) * 8]) + " ";
                    }

                    if (_Butt[15 + (jj - 1) * 8] < 10)//час
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[15 + (jj - 1) * 8]) + ":";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[15 + (jj - 1) * 8]) + ":";
                    }

                    if (_Butt[16 + (jj - 1) * 8] < 10)//мин
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[16 + (jj - 1) * 8]) + ":";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[16 + (jj - 1) * 8]) + ":";
                    }

                    if (_Butt[17 + (jj - 1) * 8] < 10)//сек
                    {
                        DateTime = DateTime + "0" + Convert.ToString(_Butt[17 + (jj - 1) * 8]) + ".000";
                    }
                    else
                    {
                        DateTime = DateTime + Convert.ToString(_Butt[17 + (jj - 1) * 8]) + ".000";
                    }
                    #endregion

                }
                else
                {
                    if (DateTime != "Null" && DateTime != "")
                    {
                        try
                        {

                            Mess = _Butt[11 + (jj - 1) * 8];
                            //Array.Reverse(_Butt, 12 + (jj - 1) * 8, sizeof(Int16));
                            SysNum = BitConverter.ToInt16(_Butt, 12 + (jj - 1) * 8);
                            //Array.Reverse(_Butt, 14 + (jj - 1) * 8, sizeof(Single));
                            Val = BitConverter.ToSingle(_Butt, 14 + (jj - 1) * 8);
                            // преобразование времени
                            //SqlDb.connection.Open();
                            SqlDb.CreateDbCommand("INSERT INTO PLCMessage (IDmess, Place, DTime, DTimeAck, SysID, SysNum, Mess, Message, isAck, Priority, Value) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                            SqlDb.command.Parameters.Add("IDmess", OleDbType.Decimal).Value = IDmess;
                            SqlDb.command.Parameters.Add("Place", OleDbType.VarChar).Value = "PLC";
                            SqlDb.command.Parameters.Add("DTime", OleDbType.DBTimeStamp).Value = DateTime;


                            if (SysID < 10)
                            {
                                if (Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck == 1)
                                {
                                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = Global.STRNOTACK;
                                }
                                else
                                {
                                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                                }
                            }
                            else if ((SysID >= 10) && (SysID < 100))
                            {
                                if (Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck == 1)
                                {
                                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = Global.STRNOTACK;
                                }
                                else
                                {
                                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                                }
                            }
                            else
                            {
                                if (Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).isAck == 1)
                                {
                                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = Global.STRNOTACK;
                                }
                                else
                                {
                                    SqlDb.command.Parameters.Add("DTimeAck", OleDbType.VarChar).Value = "";
                                }
                            }
                            SqlDb.command.Parameters.Add("SysID", OleDbType.Integer).Value = SysID;
                            SqlDb.command.Parameters.Add("SysNum", OleDbType.Integer).Value = SysNum;
                            SqlDb.command.Parameters.Add("Mess", OleDbType.Integer).Value = Mess;

                            if (SysID < 10)
                            {
                                SqlDb.command.Parameters.Add("Message", OleDbType.VarChar).Value = Objects.getObject("00" + Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Mess;
                                SqlDb.command.Parameters.Add("isAck", OleDbType.Integer).Value = Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck;
                                SqlDb.command.Parameters.Add("Priority", OleDbType.Integer).Value = Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Priority;
                                //MessageBox.Show(Convert.ToString(Objects.getObject("00" + Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Mess));
                                // MessageBox.Show(Convert.ToString(Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck));
                                // MessageBox.Show(Convert.ToString(Messages.getMessage("00" + Convert.ToString(SysID) + Convert.ToString(Mess)).Priority));
                            }
                            else if ((SysID >= 10) && (SysID < 100))
                            {
                                SqlDb.command.Parameters.Add("Message", OleDbType.VarChar).Value = Objects.getObject("0" + Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).Mess;
                                SqlDb.command.Parameters.Add("isAck", OleDbType.Integer).Value = Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).isAck;
                                SqlDb.command.Parameters.Add("Priority", OleDbType.Integer).Value = Messages.getMessage("0" + Convert.ToString(SysID) + Convert.ToString(Mess)).Priority;
                            }
                            else
                            {
                                SqlDb.command.Parameters.Add("Message", OleDbType.VarChar).Value = Objects.getObject(Convert.ToString(SysID) + Convert.ToString(SysNum)).Name + ". " + Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).Mess;
                                SqlDb.command.Parameters.Add("isAck", OleDbType.Integer).Value = Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).isAck;
                                SqlDb.command.Parameters.Add("Priority", OleDbType.Integer).Value = Messages.getMessage(Convert.ToString(SysID) + Convert.ToString(Mess)).Priority;
                            }
                            SqlDb.command.Parameters.Add("Value", OleDbType.VarChar).Value = Val;

                            SqlDb.command.ExecuteNonQuery();

                        }
                        catch (Exception e)
                        {
                            SendLogFunc(e.Message);
                        }
                    }
                }
            }
            //MessageBox.Show(DT + "-" + Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond));
            return DateTime;


        }


        private void PLCtoDB(object dev)
        {
            SendLogFunc("Запуск основного потока сбора данных с ПЛК"); SendDataToHMI(1, 0, "Нет связи");
            

            Sqldbconnection SqlDb;
            int i = 0;
            modbus modbusTCP = null;                                                                //Объект типа Модбас
            modbusTCP = new modbus();
            IP_PLC_Current= ConfigurationManager.AppSettings.Get("IP_PLC_Primary_1");
            ushort port = 502;
            ushort ID = 3;                                                                          //3 функция модбас
            byte unit = 1;                                                                          //ID устройства


            string connectionString = "Provider=" + ConfigurationManager.AppSettings.Get("Provider") +
                             ";Data Source=" + ConfigurationManager.AppSettings.Get("Data Source") +
                             ";Initial Catalog=" + ConfigurationManager.AppSettings.Get("Initial Catalog") +
                             ";Connect Timeout=" + ConfigurationManager.AppSettings.Get("Connect Timeout") +
                             ";Integrated Security=" + ConfigurationManager.AppSettings.Get("Integrated Security");
            //MessageBox.Show(connectionString);
            SqlDb = new Sqldbconnection(connectionString);
            Dictionary<string, string> Objects = new Dictionary<string, string>();                  //Словарь для объектов чтобы помнить
            Dictionary<int, ushort> Butt = new Dictionary<int, ushort>();                           //Словарь для хранения бочек и их адресов

            byte NOldButt = 1;                                                                      //Номер последней считанной бочки
            byte NNewButt = 1;                                                                      //Номер новой бочки
            Int32 NOldMess = 0;                                                                     //Номер последнего прочитанного сообщения глобальный
            Int32 NNewMess = 0;                                                                     //Номер нового сообщения глобальный
            byte NOldMess_loc = 0;                                                                  //Номер последнего прочитанного сообщения локальный (макс = Нсооб*Нбочки)
            byte NNewMess_loc = 0;                                                                  //Номер нового сообщения лок4альный
            byte NOldMess_butt = 0;                                                                 //Номер последнего прочитанного сообщения в бочке
            byte NNewMess_butt = 0;                                                                 //Номер нового сообщения в бочке
            string DateTime = "Null";                                                               //Метка времени
            byte[] _Butt = new byte[250];                                                           //Массив данных получаемых по Модбас
            Int32 i_Start;                                                                          //Старт для цикла
            Int32 i_Stop;                                                                           //Стоп для цикла
            byte[] Nmessout = new byte[18];                                                         //Массив для выдачи сообщений о пропусках
            modbusTCP.OnException += new modbus.ExceptionData(MBmaster_OnException);
            while (!SqlDb.Sqlconnected())
            {
                try
                {
                    SendDataToHMI(1, 6, "Подключение...");
                    SqlDb.open();
                    if (SqlDb.Sqlconnected()) { SendDataToHMI(1, 6, "Подключен"); }
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    SendDataToHMI(1, 6, "Нет связи");
                    SendLogFunc("Исключение при подключение к БД: " + ex.Message);
                    Thread.Sleep(1000);
                    continue;
                }

                // Проверяем наличие таблицы PLCMessage 
                try
                {
                    string NameTable = "SELECT Top 1 * FROM PLCMessage";

                    // посылаем запрос comand к БД ...результат в dr
                    SqlDb.CreateDbCommand(NameTable);
                    SqlDb.ExecuteReader();

                }
                catch 
                {
                    // Если таблицы нет, то создаем ее
                    MessageBox.Show("PLCMessage НЕ cуществует, создаем ее");
                    SqlDb.CreateDbCommand("CREATE TABLE " +
                                        "PLCMessage (ID decimal(10,0) not null" +
                                        ", IDmess decimal(10,0) not null" +
                                        ", UserName nvarchar(150) not null" +
                                        ", Place nvarchar(150) not null" +
                                        ", DTime datetime not null" +
                                        ", DTimeAck nvarchar(150) not null" +
                                        ", SysID decimal(3,0) not null" +
                                        ", SysNum decimal(3,0) not null" +
                                        ", Mess decimal(3,0) not null" +
                                        ", Message nvarchar(300) not null" +
                                        ", isAck decimal(1,0) not null" +
                                        ", Priority decimal(1,0) not null," +
                                        "Value nvarchar(150) not null)");
                    // Посылаем запрос
                    try
                    {
                        SqlDb.command.ExecuteNonQuery();
                        MessageBox.Show("Таблица создана успешно");
                    }
                    catch
                    {
                        MessageBox.Show("Ошибка при создании таблицы");
                        SendLogFunc("Ошибка при создании таблицы");
                    }
                }

                // Проверяем размер таблицы
                SqlDb.CreateDbCommand("sp_spaceused PLCMessage");
                SqlDb.ExecuteReader();

                if (SqlDb.reader.HasRows)
                {
                    while (SqlDb.reader.Read())
                    {
                        SendDataToHMI(1, 7, Convert.ToString(SqlDb.reader["Data"]));
                    }
                }
                SqlDb.reader.Close();

                
            }


            // ####################################################################################################################################
            // Определяем начальные адресса в контроллере МПСА, с которых начинаем считывать информацию. Начальный адрес MessageBank###############
            // На текущий момент стартовые адреса следующие:
            // 2001 , 2126, 2251 , 2376 , 2501 , 2626 , 2751 , 2876
            // всего банков сообщений 8

            SqlDb.CreateDbCommand("SELECT NButt, AdrButt FROM ButtSetup");
            SqlDb.ExecuteReader();
            while (SqlDb.reader.Read())
            {
                Butt[Convert.ToByte(SqlDb.reader["NButt"])] = (ushort)Convert.ToUInt16(SqlDb.reader["AdrButt"]);
            }
            SqlDb.reader.Close();
            // ###################################################################################################################################
            // читаем все наименование типов
            SqlDb.CreateDbCommand("SELECT SysID, SysNum, Name FROM Object");
            SqlDb.ExecuteReader();
            while (SqlDb.reader.Read())
            {
                Objects[Convert.ToString(SqlDb.reader["SysID"]) + Convert.ToString(SqlDb.reader["SysNum"])] = Convert.ToString(SqlDb.reader["Name"]);
            }
            SqlDb.reader.Close();
            // ###################################################################################################################################
            #region ищем последнее сообщение в журнале PLCMessage от ПЛК по параметру IDmess
            /*SqlDb.CreateDbCommand("SELECT TOP (1) IDmess FROM PLCMessage WHERE (Place = 'PLC') ORDER BY ID DESC");                        
            SqlDb.ExecuteReader();
            //string DT;
            //int qqq = 0;
            while (SqlDb.reader.Read())                        
            {
                if (SqlDb.reader["IDmess"] != null)                            
                {                                
                    NOldMess = Convert.ToInt32(SqlDb.reader["IDmess"]);
                    if (NOldMess == 0)
                    {
                        NOldMess_loc = 0; //Номер последнего прочитанного сообщения локальный (макс = Нсооб*Нбочки)
                        NOldMess_butt = 0;//Номер последнего прочитанного сообщения в бочке
                        NOldButt = 1; //Номер последней считанной бочки
                    }
                    else
                    {
                        NOldMess_loc = (byte)((NOldMess) - ((NOldMess) / 240) * 240);
                        if (NOldMess_loc == 0)
                        {
                            NOldMess_butt = 0;
                            NOldButt = 1;
                        }
                        else
                        {
                            if ((NOldMess_loc - (NOldMess_loc / 30) * 30) == 0)
                            {
                                NOldButt = (byte)(NOldMess_loc / 30);
                            }
                            else
                            {
                                NOldButt = (byte)((NOldMess_loc / 30) + 1);
                            }
                            NOldMess_butt = (byte)(NOldMess_loc - (NOldMess_loc / 30) * 30);
                        }
                    }
                }                            
                else                            
                {                                
                    NOldMess = 0;                               
                    NOldMess_loc = 0;                                
                    NOldMess_butt = 0;                                
                    NOldButt = 1;                            
                }                        
            }         
            SqlDb.reader.Close();*/
            #endregion
            // стартуем по умолчанию с данными не смотря на базу данных
            NOldMess = 0;
            NOldMess_loc = 0;
            NOldMess_butt = 0;
            NOldButt = 1;


            while (true)
            {
                #region Открываем чтение БД
                try
                {
                    if (!SqlDb.Sqlconnected())
                    {
                        SendDataToHMI(1, 5, "Подключение...");
                        SqlDb.open();
                    }
                    if (SqlDb.Sqlconnected()) { SendDataToHMI(1, 5, "Подключен"); }
                }
                catch (Exception ex)
                {
                    SendDataToHMI(1, 5, "Нет связи");
                    SendLogFunc("Нет связи с БД истории сообщений");
                    SendLogFunc(ex.Message);
                    continue;
                }
                // Открываем порт modbusTcp
                #endregion
                #region Открываем порт для чтения с ПЛК
                try
                {
                    if (!modbusTCP.Socketconnected())
                    {
                        SendLogFunc("Установление связи с ПЛК IP = " + IP_PLC_Current); SendDataToHMI(1, 1, IP_PLC_Current);
                        modbusTCP.connect(IP_PLC_Current, port);
                        SendDataToHMI(1, 0, "Открытие сокета");
                    }

                }
                catch(Exception ex)
                {
                    SendLogFunc(ex.Message);
                    continue;
                }
                #endregion

                if (modbusTCP.Socketconnected())
                {
                    

                    //if (d.getProperty(Global.TYPE) == Global.PLC) i++;
                    modbusTCP.frame = 0;
                    //string DT = Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond);
                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[NOldButt], 125, ref _Butt); 
                    //MessageBox.Show(Convert.ToString(Butt[NOldButt]));
                    //MessageBox.Show(DT + "-" + Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond)); 
                    if (_Butt == null) continue;
                    SendDataToHMI(1, 0, "Запрос на ПЛК");
                    NNewMess = BitConverter.ToInt32(_Butt, 0);
                    if (NNewMess == 0)
                    {
                        NNewMess_loc = 0;
                        NNewMess_butt = 0;
                        NNewButt = 1;
                    }
                    else
                    {
                        //MessageBox.Show(Convert.ToString(NNewMess));
                        NNewMess_loc = (byte)((NNewMess) - ((NNewMess) / 240) * 240);
                        if (NNewMess_loc == 0)
                        {
                            NNewMess_butt = 0;
                            NNewButt = 1;
                        }
                        else
                        {
                            if ((NNewMess_loc - (NNewMess_loc / 30) * 30) == 0)
                            {
                                NNewButt = (byte)(NNewMess_loc / 30);
                                NNewMess_butt = 30;
                            }
                            else
                            {
                                NNewButt = (byte)((NNewMess_loc / 30) + 1);
                                NNewMess_butt = (byte)(NNewMess_loc - (NNewMess_loc / 30) * 30);
                            }

                        }
                    }


                    // NNewMess - NOldMess, если сообщений больше чем 240.
                    if ((NNewMess - NOldMess) >= 240)
                    {
                        #region есть потерянные сообщения, а так же чтение журнала
                        // запись в журнал, о том, что определенное кол-во сообщений потеряно! нужно переделать в журнал внутрений
                        /*Nmessout[10] = 254; // SysId
                        Nmessout[11] = 1; // Mess
                        Array.Copy(BitConverter.GetBytes((Int16)1), 0, Nmessout, 12, 2);
                        Array.Copy(BitConverter.GetBytes((Single)((NNewMess - NOldMess) - 240)), 0, Nmessout, 14, 4);
                        DateTime = WriteMessToDB(SqlDb, 1, 1, Nmessout, DateTime, NNewMess, Objects);*/

                        //string DT = Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond);
                        if ((NNewButt == (byte)Butt.Count) && (NNewMess_butt == 30))
                        {
                            for (int j = 1; j <= Butt.Count; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                //qqq++;
                                if (_Butt == null) continue;
                                DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                            }
                        }
                        else
                        {
                            if (NNewMess_butt == 30)
                            {
                                i_Start = NNewButt + 1;
                                i_Stop = Butt.Count;
                            }
                            else
                            {
                                i_Start = NNewButt;
                                i_Stop = Butt.Count;
                            }

                            for (int j = i_Start; j <= i_Stop; j++)
                            {

                                if (j > NNewButt)
                                {

                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;

                                    //qqq++;
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, NNewMess_butt + 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }

                            }

                            i_Start = 1;
                            i_Stop = NNewButt;

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                //qqq++;
                                if (_Butt == null) continue;
                                if (j < NNewButt)
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                        //MessageBox.Show(DT + "-" + Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond) + "--" + Convert.ToString(qqq));
                        //qqq = 0;
                    }
                    #endregion  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    else if ((NNewMess - NOldMess) != 0)
                    {
                        #region чтение журнала. стандартный режим работы
                        if ((NOldButt == Butt.Count) && (NOldMess_butt == 30))
                        {
                            i_Start = 1;
                            i_Stop = NNewButt;

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                if (_Butt == null) continue;
                                if (j < NNewButt)
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                        else if (NNewMess_loc > NOldMess_loc)
                        {
                            if (NOldMess_butt == 30)
                            {
                                i_Start = NOldButt + 1;
                                i_Stop = NNewButt;
                            }
                            else
                            {
                                i_Start = NOldButt;
                                i_Stop = NNewButt;
                            }
                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                if ((j > NOldButt) && (j < NNewButt))
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else if ((j == NOldButt) && (j == NNewButt))
                                {
                                    DateTime = WriteMessToDB(SqlDb, NOldMess_butt + 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                                else if ((j == NOldButt))
                                {
                                    DateTime = WriteMessToDB(SqlDb, NOldMess_butt + 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else if ((j == NNewButt))
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                        else if (NNewMess_loc < NOldMess_loc)
                        {
                            if (NOldMess_butt == 30)
                            {
                                i_Start = NOldButt + 1;
                                i_Stop = Butt.Count;
                            }
                            else
                            {
                                i_Start = NOldButt;
                                i_Stop = Butt.Count;
                            }

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                if (j > NOldButt)
                                {
                                    modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                    if (_Butt == null) continue;
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, NOldMess_butt + 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                            }

                            i_Start = 1;
                            i_Stop = NNewButt;

                            for (int j = i_Start; j <= i_Stop; j++)
                            {
                                modbusTCP.ReadHoldingRegister(ID, unit, Butt[j], 125, ref _Butt); Modbus_Count_All++;
                                if (_Butt == null) continue;
                                if (j < NNewButt)
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, 30, _Butt, DateTime, NNewMess, Objects);
                                }
                                else
                                {
                                    DateTime = WriteMessToDB(SqlDb, 1, NNewMess_butt, _Butt, DateTime, NNewMess, Objects);
                                }
                            }
                        }
                    }
                    #endregion

                    NOldButt = NNewButt;
                    NOldMess = NNewMess;
                    NOldMess_butt = NNewMess_butt;
                    NOldMess_loc = NNewMess_loc;
                    Thread.Sleep(100);

                    
                    Modbus_Count_All=modbusTCP.CountAll_Upr;
                    Modbus_Count_Good = modbusTCP.CountOK_Upr;
                    Modbus_Count_Error =modbusTCP.CountError_Upr;


                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SendDataToHMI(1, 2, Convert.ToString(Modbus_Count_All));
            SendDataToHMI(1, 3, Convert.ToString(Modbus_Count_Good));
            SendDataToHMI(1, 4, Convert.ToString(Modbus_Count_Error));
        }

        private void dataGridView_diag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
