﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace PlcMsgTransfer
{
    public class modbus
    {
        // Статистика
        public static uint CountAll;
        public static uint CountError;
        public static uint CountOk;
        public uint CountAll_Upr
        {
            get { return CountAll; }
            set { CountAll = value; }
        }

        public uint CountError_Upr
        {
            get { return CountError; }
            set { CountError = value; }
        }
        public uint CountOK_Upr
        {
            get { return CountOk; }
            set { CountOk = value; }
        }







        // Constants for access


        private const byte fctReadHoldingRegister = 3;
        private const byte MODBUS_FRAME_RTU = 1;
        public UInt16 frame = 0;
        public Int32 datasize;

        public const byte excIllegalFunction = 1;
        public const byte excIllegalDataAdr = 2;
        public const byte excIllegalDataVal = 3;
        public const byte excSlaveDeviceFailure = 4;
        public const byte excAck = 5;
        public const byte excSlaveIsBusy = 6;
        public const byte excGatePathUnavailable = 10;
        public const byte excExceptionNotConnected = 253;
        public const byte excExceptionConnectionLost = 254;
        public const byte excExceptionTimeout = 255;
        private const byte excExceptionOffset = 128;
        private const byte excSendFailt = 100;

        private static ushort _timeout = 5000;
        private static ushort _refresh = 10;
        private static bool _connected = false;
        private int _count = 0;
        private byte[] data;

        private Socket tcpSynCl;
        private byte[] tcpSynClBuffer = new byte[2048];

        private static UInt16[] tbl_CRC16_Hi =  
        {
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
            0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
        };

        private static UInt16[] tbl_CRC16_Lo =  
        {
            0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2,
            0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04,
            0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
            0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8,
            0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
            0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
            0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6,
            0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10,
            0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
            0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
            0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE,
            0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
            0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA,
            0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C,
            0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
            0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0,
            0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62,
            0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
            0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE,
            0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
            0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
            0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C,
            0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76,
            0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
            0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
            0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54,
            0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
            0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98,
            0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A,
            0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
            0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86,
            0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 0x40
        };

        
        public delegate void ResponseData(ushort id, byte unit, byte function, byte[] data);
        public event ResponseData OnResponseData;
        public delegate void ExceptionData(ushort id, byte unit, byte function, byte exception);
        public event ExceptionData OnException;

        private static UInt16 CRC16(byte[] buf, UInt16 len)
        {
            UInt16 index;
            UInt16 CRC16_Hi = 0xFF;
            UInt16 CRC16_Lo = 0xFF;
            for (UInt16 i = 0; i < len; i++)
            {
                index = (UInt16)(CRC16_Hi ^ buf[i]);
                CRC16_Hi = (UInt16)(CRC16_Lo ^ tbl_CRC16_Hi[index]);
                CRC16_Lo = tbl_CRC16_Lo[index];
            }

            return (UInt16)(CRC16_Hi << 8 | CRC16_Lo);
        }
        
        public ushort timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public ushort refresh
        {
            get { return _refresh; }
            set { _refresh = value; }
        }

        public bool connected
        {
            get { return _connected; }
        }

        public int count
        {
            get { return _count; }
        }
        public bool Socketconnected()
        {
            bool value = false;
            _count++;
            if ((tcpSynCl != null) && (tcpSynCl.Connected))
            {
                if (tcpSynCl.Poll(1000, SelectMode.SelectWrite))
                {
                    value = true;
                }
                if (tcpSynCl.Poll(1000, SelectMode.SelectRead))
                {
                    value = true;
                }
                if (tcpSynCl.Poll(1000, SelectMode.SelectError))
                {
                    value = false;
                }

                return value;
            }
            else
            {
                return value;
            }
        }
        public modbus()
        {
        }

        public void connect(string ip, ushort port)
        {
            try
            {
                IPAddress _ip;
                IAsyncResult result;
                if (IPAddress.TryParse(ip, out _ip) == false)
                {
                    IPHostEntry hst = Dns.GetHostEntry(ip);
                    ip = hst.AddressList[0].ToString();
                }
                // ----------------------------------------------------------------
                // Connect asynchronous client
                tcpSynCl = new Socket(IPAddress.Parse(ip).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                result = tcpSynCl.BeginConnect(ip, port, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(10000, true);
                if (!success)
                {
                    CallException(0xFF, 0xFF, 0xFF, excExceptionConnectionLost);
                    //throw new SystemException("Connection is Lost");
                }
                else
                {
                    tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, _timeout);
                    tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, _timeout);
                    tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);
                    _connected = true;
                }
            }
            catch (System.IO.IOException error)
            {
                _connected = false;
                throw (error);  
            }
        }

        public void disconnect()
        {
            Dispose();
        }

        ~modbus()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (tcpSynCl != null)
            {
                if (tcpSynCl.Connected)
                {
                    try { tcpSynCl.Shutdown(SocketShutdown.Both); }
                    catch { }
                    tcpSynCl.Close();
                }
                tcpSynCl = null;
            }
        }

        public void ReadHoldingRegister(ushort id, byte unit, ushort startAddress, ushort numInputs, ref byte[] values)
        {
            CountAll++;
            //string DT = Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond);
            values = WriteSyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadHoldingRegister), id);
            //MessageBox.Show(DT + "-" + Convert.ToString(System.DateTime.Now.Second) + "." + Convert.ToString(System.DateTime.Now.Millisecond));
        }

        internal void CallException(ushort id, byte unit, byte function, byte exception)
        {
            if (tcpSynCl == null) return;
            if (exception == excExceptionConnectionLost)
            {
                tcpSynCl = null;
            }
            if (OnException != null) OnException(id, unit, function, exception);
        }

 /*       internal static UInt16 SwapUInt16(UInt16 inValue)
        {
            return (UInt16)(((inValue & 0xff00) >> 8) |
                     ((inValue & 0x00ff) << 8));
        }
*/
        private byte[] CreateReadHeader(ushort id, byte unit, ushort startAddress, ushort length, byte function)
        {
            byte[] data = new byte[12];
            UInt16 CRC;
            if (frame == MODBUS_FRAME_RTU)
            {
                data[0] = unit;
                data[1] = function;
                byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
                data[2] = _adr[0];
                data[3] = _adr[1];
                byte[] _length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)length));
                data[4] = _length[0];
                data[5] = _length[1];
                CRC = CRC16(data, 6);
                byte[] _CRC = BitConverter.GetBytes((short)CRC);
                data[6] = _CRC[1];
                data[7] = _CRC[0];
                datasize = 8;

            }
            else
            {
                byte[] _id = BitConverter.GetBytes((short)id);
                data[0] = _id[1];			    // Slave id high byte
                data[1] = _id[0];				// Slave id low byte
                data[5] = 6;					// Message size
                data[6] = unit;					// Slave address
                data[7] = function;				// Function code
                byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
                data[8] = _adr[0];				// Start address
                data[9] = _adr[1];				// Start address
                byte[] _length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)length));
                data[10] = _length[0];			// Number of data to read
                data[11] = _length[1];			// Number of data to read
                datasize = 12;
            }

            return data;
        }

        private byte[] WriteSyncData(byte[] write_data, ushort id)
        {

            if ((tcpSynCl != null) && (tcpSynCl.Connected))
            {
                try
                {
                    tcpSynCl.Send(write_data, 0, write_data.Length, SocketFlags.None);
                    int result = tcpSynCl.Receive(tcpSynClBuffer, 0, tcpSynClBuffer.Length, SocketFlags.None);
                    byte unit = tcpSynClBuffer[6];
                    byte function = tcpSynClBuffer[7];
                    byte[] data;

                    if (result == 0) { CallException(id, unit, write_data[7], excExceptionConnectionLost); CountError++; };

                    // ------------------------------------------------------------
                    // Response data is slave exception
                    if (function > excExceptionOffset)
                    {
                        function -= excExceptionOffset;
                        CallException(id, unit, function, tcpSynClBuffer[8]);
                        CountError++;
                        return null;
                    }
                    // ------------------------------------------------------------
                    // Read response data
                    else
                    {
                        if (frame == MODBUS_FRAME_RTU)
                        {
                            data = new byte[tcpSynClBuffer[2]];
                            for (int i = 0; i < tcpSynClBuffer[2]; i++)
                            {
                                if (i % 2 == 0)
                                {
                                    data[i] = tcpSynClBuffer[3 + i + 1];
                                }
                                else
                                {
                                    data[i] = tcpSynClBuffer[3 + i - 1];
                                }
                            }
                            //Array.Copy(tcpSynClBuffer, 3, data, 0, tcpSynClBuffer[2]);
                        }
                        else
                        {
                            data = new byte[tcpSynClBuffer[8]];
                            for (int i = 0; i < tcpSynClBuffer[8]; i++)
                            {
                                if (i % 2 == 0)
                                {
                                    data[i] = tcpSynClBuffer[9 + i + 1];
                                }
                                else
                                {
                                    data[i] = tcpSynClBuffer[9 + i - 1];
                                }
                            }
                            //Array.Copy(tcpSynClBuffer, 9, data, 0, tcpSynClBuffer[8]);
                        }
                    }
                    CountOk++;
                    return data;
                }
                catch (SystemException)
                {
                    CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
                    CountError++;
                }
            }
            else { CallException(id, write_data[6], write_data[7], excExceptionConnectionLost); CountError++; }
            return null;
        }
    }
}
